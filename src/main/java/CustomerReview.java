/**
 * Created by Semenkov on 09.04.2016.
 */
public class CustomerReview {
    private int id;
    private String customerName;
    private String customerSurName;
    private int customerAge;
    private String review;
    private int marktill1to5;
    private boolean positiveOrNegative = false;

    public CustomerReview(String customerName, String customerSurName, int customerAge, String review, int marktill1to5, boolean positiveOrNegative) {
        this.customerName = customerName;
        this.customerSurName = customerSurName;
        this.customerAge = customerAge;
        this.review = review;
        this.marktill1to5 = marktill1to5;
        this.positiveOrNegative = positiveOrNegative;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurName() {
        return customerSurName;
    }

    public void setCustomerSurName(String customerSurName) {
        this.customerSurName = customerSurName;
    }

    public int getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(int customerAge) {
        this.customerAge = customerAge;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getMarktill1to5() {
        return marktill1to5;
    }

    public void setMarktill1to5(int marktill1to5) {
        this.marktill1to5 = marktill1to5;
    }

    public boolean isPositiveOrNegative() {
        return positiveOrNegative;
    }

    public void setPositiveOrNegative(boolean positiveOrNegative) {
        this.positiveOrNegative = positiveOrNegative;
    }
}
