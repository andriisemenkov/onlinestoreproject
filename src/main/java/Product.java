/**
 * Created by Semenkov on 09.04.2016.
 */
public class Product {
    private int id;
    protected String name;
    protected int cost;
    protected boolean avaliable;
    protected String location;
    protected String rewiev;


    public Product(String name, int cost, boolean avaliable, String location, String rewiev) {
        this.name = name;
        this.cost = cost;
        this.avaliable = avaliable;
        this.location = location;
        this.rewiev = rewiev;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isAvaliable() {
        return avaliable;
    }

    public void setAvaliable(boolean avaliable) {
        this.avaliable = avaliable;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRewiev() {
        return rewiev;
    }

    public void setRewiev(String rewiev) {
        this.rewiev = rewiev;
    }
}
