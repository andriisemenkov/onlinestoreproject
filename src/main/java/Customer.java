/**
 * Created by Semenkov on 09.04.2016.
 */
public class Customer {
    private int id;
    private double cost;
    private Product product;
    private CustomerReview customerReview;
    private Store store;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public CustomerReview getCustomerReview() {
        return customerReview;
    }

    public void setCustomerReview(CustomerReview customerReview) {
        this.customerReview = customerReview;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Customer(int id, double cost, Product product, CustomerReview customerReview, Store store) {
        this.id = id;
        this.cost = cost;
        this.product = product;
        this.customerReview = customerReview;
        this.store = store;
    }
}
