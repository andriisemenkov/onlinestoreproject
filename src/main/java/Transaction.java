/**
 * Created by Semenkov on 09.04.2016.
 */
public class Transaction {
    private int id;
    private Product product;
    private Store store;
    private CustomerReview customerReview;
    private boolean done = false;
    private ProductList productList;

    public Transaction(Product product, Store store, CustomerReview customerReview) {
        this.product = product;
        this.store = store;
        this.customerReview = customerReview;
        done = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public CustomerReview getCustomerReview() {
        return customerReview;
    }

    public void setCustomerReview(CustomerReview customerReview) {
        this.customerReview = customerReview;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
