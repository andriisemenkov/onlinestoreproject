/**
 * Created by Semenkov on 09.04.2016.
 */
public class Store {
    private int id;
    private String location;
    private int numberOfWorkers;
    private String storeName;
    private ProductList productList;
    private String customersReviews;

    public Store() {
    }

    public Store(String location, int numberOfWorkers, String storeName, ProductList productList, String customersReviews) {
        this.location = location;
        this.numberOfWorkers = numberOfWorkers;
        this.storeName = storeName;
        this.productList = productList;
        this.customersReviews = customersReviews;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getNumberOfWorkers() {
        return numberOfWorkers;
    }

    public void setNumberOfWorkers(int numberOfWorkers) {
        this.numberOfWorkers = numberOfWorkers;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public ProductList getProductList() {
        return productList;
    }

    public void setProductList(ProductList productList) {
        this.productList = productList;
    }

    public String getCustomersReviews() {
        return customersReviews;
    }

    public void setCustomersReviews(String customersReviews) {
        this.customersReviews = customersReviews;
    }
}
